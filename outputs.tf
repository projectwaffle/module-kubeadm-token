output "token" {
  description = "Kubeadm token"
  value       = "${join(".", list(join("", random_shuffle.token1.result)), list(join("", random_shuffle.token2.result)))}"
}
