# AWS to create kubeadm token for kubernetes creation Terraform module

[![CircleCI](https://circleci.com/bb/projectwaffle/module-kubeadm-token.svg?style=svg)](https://circleci.com/bb/projectwaffle/module-kubeadm-token/)

Terraform module which creates Kubeadm token. Might be used for different modules

## Usage

```hcl
module "kubeadm-token" {
  source = "bitbucket.org/projectwaffle/module-kubeadm-token.git?ref=tags/v0.0.1"
}
```

## Terraform version

Terraform version 0.11.10 or newer is required for this module to work.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Outputs

| Name | Description |
|------|-------------|
| token | Kubeadm token |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Authors

Module is maintained by [Sergiu Plotnicu](https://bitbucket.org/projectwaffle/)

## License

Licensed under Sergiu Plotnicu, please contact him at - sergiu.plotnicu@gmail.com


